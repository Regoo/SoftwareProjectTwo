package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CourseDB 
{
	private Course course;
	
	public static void connection() throws ClassNotFoundException
	{
		Class.forName("com.mysql.jdbc.Driver");
	}
	
	public void addCourse(Course x) throws ClassNotFoundException
	{
		connection();
		String url="jdbc:mysql://localhost/softwareproject";
		String name="root";
		String pass="";
		Connection con;
		try {
			con = DriverManager.getConnection(url,name,pass );
			String queryCheck1 = "SELECT * from course" ;
			Statement st1 = con.createStatement();
			ResultSet rs1 = st1.executeQuery(queryCheck1); // execute the query, and get a java resultset
			int count = 0;
		      while (rs1.next()) {
		        count++;
		      }
		      count++;
			String sql="insert into course (ID,name,describtion,Email)"
					+ "values("+count+",\""+x.getName()+"\",\""+x.getDiscribtion()+"\",\""+x.getEmail()+"\")";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.execute();
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public ArrayList<Course> selectCourses() throws ClassNotFoundException
	{
		
		ArrayList<Course> ar=new ArrayList<Course>();
		connection();
		String url="jdbc:mysql://localhost/softwareproject";
		String name="root";
		String pass="";
		Connection con;
		try {
			con = DriverManager.getConnection(url,name,pass );
			String queryCheck = "SELECT * from course" ;
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(queryCheck); // execute the query, and get a java resultset
			while (rs.next()) {
				Course x=new Course();
				x.setEmail(rs.getString("Email"));
				x.setName(rs.getString("name"));
				x.setDiscribtion(rs.getString("describtion"));
				x.setId(rs.getInt("ID"));
				ar.add(x);
		      }
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ar;
	}
	
	public Course selectCourse(String Name) throws ClassNotFoundException
	{
		Course x=new Course ();
		connection();
		String url="jdbc:mysql://localhost/softwareproject";
		String name="root";
		String pass="";
		Connection con;
		try {
			con = DriverManager.getConnection(url,name,pass );
			String queryCheck = "SELECT * from course WHERE name = \"" + Name +"\"" ;
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(queryCheck); // execute the query, and get a java resultset
			while(rs.next())
			{
				x.setEmail(rs.getString("Email"));
				x.setName(rs.getString("name"));
				x.setDiscribtion(rs.getString("describtion"));
				x.setId(rs.getInt("ID"));
			}
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return x;
	}

	
	
	
	
	public CourseDB()
	{
		course=new Course();
	}
	
	public CourseDB(Course course) {
		super();
		this.course = course;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}
	

}
