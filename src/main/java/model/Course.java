package model;

public class Course 
{
	private int id;
	private String name;
	private String discribtion;
	private String Email;
	
	public Course(int id, String name, String discribtion) {
		super();
		this.id = id;
		this.name = name;
		this.discribtion = discribtion;
	}
	
	public Course() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDiscribtion() {
		return discribtion;
	}
	public void setDiscribtion(String discribtion) {
		this.discribtion = discribtion;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

}
