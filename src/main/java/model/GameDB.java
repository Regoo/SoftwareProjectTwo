package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class GameDB 
{
	private ArrayList<Game> game;
	
	public static void connection() throws ClassNotFoundException
	{
		Class.forName("com.mysql.jdbc.Driver");
	}
	
	public void updatehighscore(int score,String name1,int id) throws ClassNotFoundException
	{
		connection();
		String url="jdbc:mysql://localhost/softwareproject";
		String name="root";
		String pass="";
		Connection con;
		try {
			con = DriverManager.getConnection(url,name,pass );
			String sql = "UPDATE `game` SET `highscore`="+score+",`NameHS`=\""+name1+"\" WHERE ID="+id;
			PreparedStatement pst=con.prepareStatement(sql);
			pst.execute();
			con.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void updatestate(int state,int id) throws ClassNotFoundException
	{
		connection();
		String url="jdbc:mysql://localhost/softwareproject";
		String name="root";
		String pass="";
		Connection con;
		try {
			con = DriverManager.getConnection(url,name,pass );
			String sql = "UPDATE `game` SET `state`="+state+" WHERE ID="+id;
			PreparedStatement pst=con.prepareStatement(sql);
			pst.execute();
			con.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void addGame(Game g) throws ClassNotFoundException
	{
		connection();
		String url="jdbc:mysql://localhost/softwareproject";
		String name="root";
		String pass="";
		Connection con;
		try {
			con = DriverManager.getConnection(url,name,pass );
			String queryCheck1 = "SELECT * from game" ;
			Statement st1 = con.createStatement();
			ResultSet rs1 = st1.executeQuery(queryCheck1); // execute the query, and get a java resultset
			int count = 0;
		      while (rs1.next()) {
		        count++;
		      }
		      count++;
		      g.setID(count);
			String sql="insert into game (ID,name,catogary,highscore,IDC,Email,NQuestion,NameHS,state)"
					+ "values("+g.getID()+",\""+g.getName()+"\",\""+g.getType()+"\","+g.getHighScore()+","+g.getIDCourse()
					+",\""+g.getEmailOfPublicher()+"\","+g.getNumberofQuestion()+",\""+"NNNN"+"\",1)";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.execute();
			con.close();
			if(g.getType().equals("TrueFalse"))
			{
				addGameTF(g);
			}
			else
			{
				addGameMCQ(g);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void addGameMCQ(Game g) throws ClassNotFoundException
	{
		connection();
		String url="jdbc:mysql://localhost/softwareproject";
		String name="root";
		String pass="";
		Connection con;
		try {
			con = DriverManager.getConnection(url,name,pass );
			for(int i=0;i<g.getMcq().size();i++)
			{
				String sql="insert into mcq (IDG,quest,ans1,ans2,ans3,ans4,ans)"
						+ "values("+g.getID()+",\""+g.getMcq().get(i).getQuestion()+"\",\""+g.getMcq().get(i).getChoice().get(0)
						+"\",\""+g.getMcq().get(i).getChoice().get(1)+"\",\""+g.getMcq().get(i).getChoice().get(2)
						+"\",\""+g.getMcq().get(i).getChoice().get(3)+"\","+g.getMcq().get(i).getAnswer()+")";
				PreparedStatement pst=con.prepareStatement(sql);
				pst.execute();
			}
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void addGameTF(Game g) throws ClassNotFoundException
	{
		connection();
		String url="jdbc:mysql://localhost/softwareproject";
		String name="root";
		String pass="";
		Connection con;
		try {
			con = DriverManager.getConnection(url,name,pass );
			for(int i=0;i<g.getTf().size();i++)
			{
				String sql="insert into tf (IDG,quest,ans)"
						+ "values("+g.getID()+",\""+g.getTf().get(i).getQuestion()+"\",\""+g.getTf().get(i).getAnswer()+"\")";
				PreparedStatement pst=con.prepareStatement(sql);
				pst.execute();
			}
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<Game> showGame(int idc) throws ClassNotFoundException
	{
		ArrayList<Game>game=new ArrayList<Game>();
		
		connection();
		String url="jdbc:mysql://localhost/softwareproject";
		String name="root";
		String pass="";
		Connection con;
		try {
			con = DriverManager.getConnection(url,name,pass );
			String queryCheck1 = "SELECT * from game where IDC = "+idc ;
			Statement st1 = con.createStatement();
			ResultSet rs1 = st1.executeQuery(queryCheck1); // execute the query, and get a java resultset
		      while (rs1.next()) {
		    	  Game g=new Game();
		        g.setID(rs1.getInt("ID"));
		        g.setName(rs1.getString("name"));
		        g.setNameHighScore(rs1.getString("NameHS"));
		        g.setType(rs1.getString("catogary"));
		        g.setHighScore(rs1.getInt("highscore"));
		        g.setIDCourse(rs1.getInt("IDC"));
		        g.setEmailOfPublicher(rs1.getString("Email"));
		        g.setNumberofQuestion(rs1.getInt("NQuestion"));
		        g.setState(rs1.getInt("state"));
		        game.add(g);
		      }
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return game;
	}
	
	public Game selectGame(String Name) throws ClassNotFoundException
	{
		Game g =new Game();
		connection();
		String url="jdbc:mysql://localhost/softwareproject";
		String name="root";
		String pass="";
		Connection con;
		try {
			con = DriverManager.getConnection(url,name,pass );
			String queryCheck = "SELECT * from game WHERE name = \"" + Name +"\"" ;
			Statement st = con.createStatement();
			ResultSet rs1 = st.executeQuery(queryCheck); // execute the query, and get a java resultset
			while(rs1.next())
			{
				g.setID(rs1.getInt("ID"));
		        g.setName(rs1.getString("name"));
		        g.setNameHighScore(rs1.getString("NameHS"));
		        g.setType(rs1.getString("catogary"));
		        g.setHighScore(rs1.getInt("highscore"));
		        g.setIDCourse(rs1.getInt("IDC"));
		        g.setEmailOfPublicher(rs1.getString("Email"));
		        g.setNumberofQuestion(rs1.getInt("NQuestion"));
		        g.setState(rs1.getInt("state"));
			}
		con.close();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		return g;
	}
	
	
	public Game selectGame(Game g) throws ClassNotFoundException
	{
		if(g.getType().equals("TrueFalse"))
		{
			g.setTf(Selecttf(g.getID()));
		}
		else
		{
			g.setMcq(Selectmcq(g.getID()));
		}
		return g;
	}
	
	public ArrayList<MCQ> Selectmcq(int idg) throws ClassNotFoundException
	{
		ArrayList<MCQ>mcq=new ArrayList<MCQ>();

		connection();
		String url="jdbc:mysql://localhost/softwareproject";
		String name="root";
		String pass="";
		Connection con;
		try {
			con = DriverManager.getConnection(url,name,pass );
			String queryCheck1 = "SELECT * from mcq where IDG =\""+idg+"\"";
			Statement st1 = con.createStatement();
			ResultSet rs1 = st1.executeQuery(queryCheck1); // execute the query, and get a java resultset
		      while (rs1.next()) {
		    	  MCQ m=new MCQ();
		    	  ArrayList<String>ch=new ArrayList<String>();
		    	  m.setQuestion(rs1.getString("quest"));
		    	  ch.add(rs1.getString("ans1"));
		    	  ch.add(rs1.getString("ans2"));
		    	  ch.add(rs1.getString("ans3"));
		    	  ch.add(rs1.getString("ans4"));
		    	  m.setAnswer(rs1.getInt("ans"));
		    	  m.setChoice(ch);
		        mcq.add(m);
		      }
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mcq;
	}
	
	public ArrayList<TrueFalse> Selecttf(int idg) throws ClassNotFoundException
	{
		ArrayList<TrueFalse>tf=new ArrayList<TrueFalse>();

		connection();
		String url="jdbc:mysql://localhost/softwareproject";
		String name="root";
		String pass="";
		Connection con;
		try {
			con = DriverManager.getConnection(url,name,pass );
			String queryCheck1 = "SELECT * from tf where IDG =\""+idg+"\"";
			Statement st1 = con.createStatement();
			ResultSet rs1 = st1.executeQuery(queryCheck1); // execute the query, and get a java resultset
		      while (rs1.next()) {
		    	  TrueFalse t=new TrueFalse();
		    	  t.setQuestion(rs1.getString("quest"));
		    	  t.setAnswer(rs1.getString("ans"));
		        tf.add(t);
		      }
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tf;
	}
	
	public ArrayList<Game> getGame() {
		return game;
	}

	public void setGame(ArrayList<Game> game) {
		this.game = game;
	}

	public GameDB(ArrayList<Game> game) {
		super();
		this.game = game;
	}
	
	public GameDB() {
		//super();
		game = new ArrayList<Game>();
	}
	
}
