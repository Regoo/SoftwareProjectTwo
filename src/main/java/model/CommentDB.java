package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CommentDB 
{
	private Comment comment;
	
	public static void connection() throws ClassNotFoundException
	{
		Class.forName("com.mysql.jdbc.Driver");
	}
	
	public void addComment(Comment x) throws ClassNotFoundException
	{
		connection();
		String url="jdbc:mysql://localhost/softwareproject";
		String name="root";
		String pass="";
		Connection con;
		try {
			con = DriverManager.getConnection(url,name,pass );
			String sql="insert into comment (IDG,text,Email)"
					+ "values("+x.getId()+",\""+x.getText()+"\",\""+x.getEmail()+"\")";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.execute();
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public ArrayList<Comment> selectComment(int idg) throws ClassNotFoundException
	{
		
		ArrayList<Comment> ar=new ArrayList<Comment>();
		connection();
		String url="jdbc:mysql://localhost/softwareproject";
		String name="root";
		String pass="";
		Connection con;
		try {
			con = DriverManager.getConnection(url,name,pass );
			String queryCheck = "SELECT * from comment WHERE IDG = \"" + idg +"\"" ;
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(queryCheck); // execute the query, and get a java resultset
			while (rs.next()) {
				Comment x=new Comment();
				x.setEmail(rs.getString("Email"));
				x.setText(rs.getString("text"));
				x.setId(rs.getInt("IDG"));
				ar.add(x);
		      }
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ar;
	}
	
	public CommentDB()
	{
		comment=new Comment();
	}

	public Comment getComment() {
		return comment;
	}

	public void setComment(Comment comment) {
		this.comment = comment;
	}
	
	

}
