package controller;

import java.util.ArrayList;

import model.Comment;
import model.CommentDB;

public class CommentDBCon 
{
	private CommentDB comment;
	
	public void addComment(Comment x) throws ClassNotFoundException
	{
		comment.addComment(x);
	}
	
	public ArrayList<Comment> selectComment(int idg) throws ClassNotFoundException
	{
		return comment.selectComment(idg);
	}
	
	public CommentDBCon()
	{
		setComment(new CommentDB());
	}

	public CommentDB getComment() {
		return comment;
	}

	public void setComment(CommentDB comment) {
		this.comment = comment;
	}

}
