package model;

public class GameTF extends Game
{
	private String Question;
	private String answer;
	public String getQuestion() {
		return Question;
	}
	public void setQuestion(String question) {
		Question = question;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public GameTF(String question, String answer) {
		//super();
		Question = question;
		this.answer = answer;
	}
	public GameTF() {
		Question="";
		answer="";
		
	}
	

}
