package model;

import java.util.ArrayList;

public class MCQ
{
	private String Question;
	private ArrayList<String>choice;
	private int answer;
	private final int mark=10;
	public MCQ() {
		Question = "";
		choice = new ArrayList<String>(4);
		answer = 0;
	}
	public String getQuestion() {
		return Question;
	}
	public void setQuestion(String question) {
		Question = question;
	}
	public ArrayList<String> getChoice() {
		return choice;
	}
	public void setChoice(ArrayList<String> choice) {
		this.choice = choice;
	}
	public int getAnswer() {
		return answer;
	}
	public void setAnswer(int answer) {
		this.answer = answer;
	}
	public int getMark()
	{
		return mark;
	}

}
