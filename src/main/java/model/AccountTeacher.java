package model;


import java.io.IOException;

import controller.AccountDBCon;
import controller.CourseDBCon;
import controller.GameDBCon;

public class AccountTeacher extends Account  //implements ACCOUNT1
{

	public AccountTeacher(String name, String email, String password,
			String country, String type, String phone, int age) 
	{
		//super(name, email, password, country, type, phone, age);
	}

	public AccountTeacher() {
		// TODO Auto-generated constructor stub
	}
	
	public void AddGame(Game g) throws IOException, ClassNotFoundException
	{
		GameDBCon game=new GameDBCon();
		game.addGame(g);
	}
	
	public void addCourse(Course course) throws ClassNotFoundException
	{
		CourseDBCon c = new CourseDBCon();
		c.addCourse(course);
	}
	
	public void copygame(int id,String email) throws ClassNotFoundException
	{
		AccountDBCon c = new AccountDBCon();
		c.copygame(id, email);
	}

}
