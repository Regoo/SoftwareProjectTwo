package model;

import java.util.ArrayList;

public class GameMCQ extends Game
{
	private String Question;
	private ArrayList<String>choice;
	private String answer;
	private final int mark=10;
	public GameMCQ() {
		Question = "";
		choice = new ArrayList<String>(4);
		answer = "";
	}
	public String getQuestion() {
		return Question;
	}
	public void setQuestion(String question) {
		Question = question;
	}
	public ArrayList<String> getChoice() {
		return choice;
	}
	public void setChoice(ArrayList<String> choice) {
		this.choice = choice;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public int getMark()
	{
		return mark;
	}

}
