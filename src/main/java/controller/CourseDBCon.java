package controller;

import java.util.ArrayList;

import model.Course;
import model.CourseDB;

public class CourseDBCon 
{
	CourseDB course;
	
	public void addCourse(Course x) throws ClassNotFoundException
	{
		course.addCourse(x);
	}
	
	public ArrayList<Course> selectCourses() throws ClassNotFoundException
	{
		return course.selectCourses();
	}
	
	public Course selectCourse(String Name) throws ClassNotFoundException
	{
		return course.selectCourse(Name);
	}
	
	public CourseDBCon()
	{
		course=new CourseDB();
	}
	
	public CourseDBCon(CourseDB course) {
		super();
		this.course = course;
	}

	public Course getCourse() {
		return course.getCourse();
	}

	public void setCourse(Course course1) {
		course.setCourse(course1);
	}
}
