package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

public class AccountDB 
{
	private Account account;

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public AccountDB(Account account) {
		//super();
		this.account = account;
	}
	
	public AccountDB() {
		//super();
		account = new Account();
		//acc=new ArrayList<Account>();
		//ReadData();
	}
	
	public static void connection() throws ClassNotFoundException
	{
		Class.forName("com.mysql.jdbc.Driver");
	}
	
	public void copygame(int id,String email) throws ClassNotFoundException
	{
		connection();
		String url="jdbc:mysql://localhost/softwareproject";
		String name="root";
		String pass="";
		Connection con;
		try {
			con = DriverManager.getConnection(url,name,pass );
			String sql="INSERT INTO `copygame`(`id`, `email`) VALUES ("+id+",\""+email+"\")";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.execute();
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public boolean CheckValidation(String Email,String password) throws ClassNotFoundException
	{
		connection();
		String url="jdbc:mysql://localhost/softwareproject";
		String name="root";
		String pass="";
		Connection con;
		try {
			con = DriverManager.getConnection(url,name,pass );
			String queryCheck = "SELECT * from user WHERE email = \"" + Email +"\""+"and password = \""+password+"\"" ;
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(queryCheck); // execute the query, and get a java resultset

			// if this ID already exists, we quit
			if(rs.absolute(1)) {
			     con.close();
			     return true ;
			}
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean CheckValidation(String Email) throws ClassNotFoundException
	{
		connection();
		String url="jdbc:mysql://localhost/softwareproject";
		String name="root";
		String pass="";
		Connection con;
		try {
			con = DriverManager.getConnection(url,name,pass );
			String queryCheck = "SELECT * from user WHERE email = \"" + Email +"\"" ;
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(queryCheck); // execute the query, and get a java resultset
			// if this ID already exists, we quit
			if(rs.absolute(1)) {
				 con.close();
			     return true ;
			}
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;

	}
	
	
	
	public void addAccount(Account x) throws ClassNotFoundException
	{
		connection();
		String url="jdbc:mysql://localhost/softwareproject";
		String name="root";
		String pass="";
		Connection con;
		try {
			con = DriverManager.getConnection(url,name,pass );
			String sql="insert into user (email,name,password,Role,Country,age,phone,state)"
					+ "values(\""+x.getEmail()+"\",\""+x.getName()+"\",\""+x.getPassword()+"\",\""+x.getRole()+"\",\""
					+x.getCountry()+"\","+x.getAge()+",\""+x.getPhone()+"\","+0+")";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.execute();
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public Account selectuser(String Email) throws ClassNotFoundException
	{
		Account x=new Account ();
		connection();
		String url="jdbc:mysql://localhost/softwareproject";
		String name="root";
		String pass="";
		Connection con;
		try {
			con = DriverManager.getConnection(url,name,pass );
			String queryCheck = "SELECT * from user WHERE email = \"" + Email +"\"" ;
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(queryCheck); // execute the query, and get a java resultset
			while(rs.next())
			{
				x.setEmail(rs.getString("email"));
				x.setName(rs.getString("name"));
				x.setPassword(rs.getString("password"));
				x.setRole(rs.getString("Role"));
				x.setAge(rs.getInt("age"));
				x.setCountry(rs.getString("Country"));
				x.setPhone(rs.getString("phone"));
				x.setState(rs.getInt("state"));
				x.setDate(rs.getTimestamp("date"));
			}
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return x;
	}
	
	public ArrayList<String> selectteacher(String email) throws ClassNotFoundException
	{
		connection();
		String url="jdbc:mysql://localhost/softwareproject";
		String name="root";
		String pass="";
		Connection con;
		ArrayList<String> Temail=new ArrayList<String>(); 
		try {
			con = DriverManager.getConnection(url,name,pass );
			String queryCheck = "SELECT * FROM `user` WHERE `Role` = \"teacher\"" ;
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(queryCheck); // execute the query, and get a java resultset
			
			while(rs.next())
			{
				if(email.equals(rs.getString("email")))
					continue;
				Temail.add(rs.getString("email"));
				
			}
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Temail;
	}
	
	public void updatestate(String email,Timestamp date) throws ClassNotFoundException
	{
		connection();
		String url="jdbc:mysql://localhost/softwareproject";
		String name="root";
		String pass="";
		Connection con;
		try {
			con = DriverManager.getConnection(url,name,pass );
			String sql="UPDATE `user` SET `state`=1,`date` =\""+date+"\" WHERE (`state`=0 and `email` =\""+email+"\")";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.execute();
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void updatestateall(Timestamp date) throws ClassNotFoundException
	{
		connection();
		String url="jdbc:mysql://localhost/softwareproject";
		String name="root";
		String pass="";
		Connection con;
		try {
			con = DriverManager.getConnection(url,name,pass );
			String sql="UPDATE `user` SET `state`=1,`date` =\""+date+"\" WHERE (`state`=0)";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.execute();
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
