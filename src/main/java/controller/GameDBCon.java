package controller;

import java.util.ArrayList;

import model.Game;
import model.GameDB;

public class GameDBCon 
{
	private GameDB game;
	
	public void updatestate(int state,int id) throws ClassNotFoundException
	{
		game.updatestate(state, id);
	}
	
	public void updatehighscore(int score,String name1,int id) throws ClassNotFoundException
	{
		game.updatehighscore(score, name1, id);
	}
	
	public void addGame(Game g) throws ClassNotFoundException
	{
		game.addGame(g);
	}
	
	public ArrayList<Game> showGame(int idc) throws ClassNotFoundException
	{
		return game.showGame(idc);
	}
	
	public Game selectGame(Game g) throws ClassNotFoundException
	{
		return game.selectGame(g);
	}
	
	public Game selectGame(String Name) throws ClassNotFoundException
	{
		return game.selectGame(Name);
	}
	
	public GameDBCon()
	{
		game=new GameDB();
	}
	
	public GameDB getGame() {
		return game;
	}

	public void setGame(GameDB game) {
		this.game = game;
	}
	

}
