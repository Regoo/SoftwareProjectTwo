package controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;

import model.Account;
import model.AccountStudent;
import model.AccountTeacher;
import model.Comment;
import model.Course;
import model.Game;

public class AccountCon 
{
	private Account account;
	AccountStudent student;
	AccountTeacher teacher;
	
	public void copygame(int id,String email) throws ClassNotFoundException
	{
		teacher.copygame(id, email);
	}
	
	public void AddGame(Game g) throws IOException, ClassNotFoundException
	{
		teacher.AddGame(g);
	}
	
	public Game PlayGame(String c) throws IOException, ClassNotFoundException
	{
		return account.PlayGame(c);
	}
	
	public ArrayList<Comment>showComments(int id) throws ClassNotFoundException
	{
		return account.showComments(id);
	}
	
	public void addCourse(Course course) throws ClassNotFoundException
	{
		teacher.addCourse(course);
	}
	
	public AccountCon(String name, String email, String password, String country,
			String type, String phone, int age) {
		account=new Account(name, email, password, country, type, phone, age);
	}
	
	public AccountCon() {
	
		account=new Account();
		student=new AccountStudent();
		teacher =new AccountTeacher();
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public void WriteComment(Comment comment) throws ClassNotFoundException
	{
		account.WriteComment(comment);
	}

	public String getName() {
		return account.getName();
	}
	public void setName(String name) {
		account.setName(name);
	}
	public String getEmail() {
		return account.getEmail();
	}
	public void setEmail(String email) {
		account.setEmail(email);
	}
	public String getPassword() {
		return account.getPassword();
	}
	public void setPassword(String password) {
		account.setPassword(password);
	}
	public String getCountry() {
		return account.getCountry();
	}
	public void setCountry(String country) {
		account.setCountry(country);
	}
	public String getRole() {
		return account.getRole();
	}
	public void setRole(String type) {
		account.setRole(type);
	}
	public String getPhone() {
		return account.getPhone();
	}
	public void setPhone(String phone) {
		account.setPhone(phone);
	}
	public int getAge() {
		return account.getAge();
	}
	public void setAge(int age) {
		account.setAge(age);
	}
	
	public Timestamp getDate() {
		return account.getDate();
	}

	public void setDate(Timestamp date) {
		account.setDate(date);
	}

	public int getState() {
		return account.getState();
	}

	public void setState(int state) {
		account.setState(state);
	}

}
