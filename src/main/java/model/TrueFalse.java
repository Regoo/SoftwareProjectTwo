package model;

public class TrueFalse 
{
	private int idg;
	private String Question;
	private String answer;
	private final int mark=10;
	public String getQuestion() {
		return Question;
	}
	public void setQuestion(String question) {
		Question = question;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public TrueFalse(String question, String answer) {
		//super();
		Question = question;
		this.answer = answer;
	}
	public TrueFalse() {
		Question="";
		answer="";
		
	}
	public int getIdg() {
		return idg;
	}
	public void setIdg(int idg) {
		this.idg = idg;
	}
	public int getMark() {
		return mark;
	}
	

}
