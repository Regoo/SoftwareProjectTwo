package model;

import java.sql.Timestamp;


public class Notify 
{
	private String id;
	private String discription;
	private Timestamp date;
	
	public Notify(String id, String discription, Timestamp date) {
		super();
		this.id = id;
		this.discription = discription;
		this.date = date;
	}
	
	public Notify() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDiscription() {
		return discription;
	}
	public void setDiscription(String discription) {
		this.discription = discription;
	}
	public Timestamp getDate() {
		return date;
	}
	public void setDate(Timestamp date) {
		this.date = date;
	}

}
