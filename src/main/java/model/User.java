package model;

import controller.AccountCon;
import controller.AccountDBCon;

public class User 
{
	private AccountCon account1;
	private Account account;
	public String namec;
	public String nameg;
	public User()
	{
		account1=new AccountCon();
	}
	
	public AccountCon getAccount1() {
		return account1;
	}

	public void setAccount1(AccountCon account) {
		this.account1 = account;
	}
	
	public String open() 
	{
		return "webpage";
	}
	
	public void pp(String c)
	{
		System.out.println(c);
	}
	
	public boolean login(String Email,String password) throws ClassNotFoundException
	{
		AccountDBCon acc =new AccountDBCon();
		if(acc.CheckValidation(Email, password))
		{
			setAccount(acc.selectuser(Email));
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean registeration(AccountCon acc) throws ClassNotFoundException
	{
		AccountDBCon acc1=new AccountDBCon();
		if(acc1.CheckValidation(acc.getEmail()))
		{
			return false;
		}
		else
		{
			acc1.addAccount(acc.getAccount());
			setAccount(acc.getAccount());
			return true;
		}
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account1) {
		this.account = account1;
	}
}