package model;

public class Comment 
{
	private int idg;
	private String text;
	private String Email;
	
	public Comment(int id, String text, String email) {
		super();
		this.idg = id;
		this.text = text;
		Email = email;
	}
	
	public Comment() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return idg;
	}
	
	public void setId(int id) {
		this.idg = id;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getEmail() {
		return Email;
	}
	
	public void setEmail(String email) {
		Email = email;
	}

}
