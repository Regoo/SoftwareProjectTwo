package controller;

import java.sql.Timestamp;
import java.util.ArrayList;

import model.Account;
import model.AccountDB;

public class AccountDBCon 
{
	AccountDB account;

	public AccountDB getAccount() {
		return account;
	}

	public void setAccount(AccountDB account) {
		this.account = account;
	}

	public AccountDBCon(AccountDB account) {
		//super();
		this.account = account;
	}
	
	public AccountDBCon() {
		//super();
		account = new AccountDB();
		
	}
	
	public void copygame(int id,String email) throws ClassNotFoundException
	{
		account.copygame(id, email);
	}
	
	public boolean CheckValidation(String Email,String password) throws ClassNotFoundException
	{
		return account.CheckValidation(Email, password);
	}
	
	public boolean CheckValidation(String Email) throws ClassNotFoundException
	{
		return account.CheckValidation(Email);
	}
	
	public void addAccount(Account x) throws ClassNotFoundException
	{
		account.addAccount(x);
	}
	
	public Account selectuser(String Email) throws ClassNotFoundException
	{
		return account.selectuser(Email);
	}
	
	public void updatestate(String email,Timestamp date) throws ClassNotFoundException
	{
		account.updatestate(email, date);
	}
	
	public void updatestateall(Timestamp date) throws ClassNotFoundException
	{
		account.updatestateall(date);
	}
	
	public ArrayList<String> selectteacher(String email) throws ClassNotFoundException
	{
		return account.selectteacher(email);
	}
}
