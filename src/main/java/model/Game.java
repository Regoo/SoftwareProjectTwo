package model;

import java.io.IOException;
import java.util.ArrayList;

public class Game 
{
	private int ID;
	private String Name;
	private String Type;
	private int IDCourse;
	private int HighScore;
	private String EmailOfPublicher;
	private String NameHighScore;
	private int numberofQuestion;
	private int state;
	private ArrayList <MCQ>mcq;
	private ArrayList<TrueFalse>tf;
	
	public String getEmailOfPublicher() {
		return EmailOfPublicher;
	}
	public void setEmailOfPublicher(String nameOfPublicher) {
		EmailOfPublicher = nameOfPublicher;
	}
	public String getNameHighScore() {
		return NameHighScore;
	}
	public void setNameHighScore(String nameHighScore) {
		NameHighScore = nameHighScore;
	}
	public int getNumberofQuestion() {
		return numberofQuestion;
	}
	public void setNumberofQuestion(int numberofQuestion) {
		this.numberofQuestion = numberofQuestion;
	}
	public int getHighScore() {
		return HighScore;
	}
	public void setHighScore(int highScore) {
		HighScore = highScore;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	

	public Game() {
		super();
		Name = "";
		Type = "";
		HighScore=0;
		mcq=new ArrayList<MCQ>();
		tf=new ArrayList<TrueFalse>();
		NameHighScore="";
		numberofQuestion=5;
	}
	public Game(int iD, String name, String type, int iDCourse,
			int highScore, String emailOfPublicher, String nameHighScore,
			int numberofQuestion, ArrayList<MCQ> mcq, ArrayList<TrueFalse> tf) {
		super();
		ID = iD;
		Name = name;
		Type = type;
		IDCourse = iDCourse;
		HighScore = highScore;
		EmailOfPublicher = emailOfPublicher;
		NameHighScore = nameHighScore;
		this.numberofQuestion = numberofQuestion;
		this.mcq = mcq;
		this.tf = tf;
	}
	public void CreateGame (User user) throws IOException
	{
		//new AddGui(user);
	}
	public void DeleteGame ()
	{
		
	}
	public void PlayGame (User user) throws IOException
	{
		//new PlayGui(user);
	}
	public void EditeGame ()
	{
		
	}
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public int getIDCourse() {
		return IDCourse;
	}
	public void setIDCourse(int iDCourse) {
		IDCourse = iDCourse;
	}
	public ArrayList <MCQ> getMcq() {
		return mcq;
	}
	public void setMcq(ArrayList <MCQ> mcq) {
		this.mcq = mcq;
	}
	public ArrayList<TrueFalse> getTf() {
		return tf;
	}
	public void setTf(ArrayList<TrueFalse> tf) {
		this.tf = tf;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	
}

