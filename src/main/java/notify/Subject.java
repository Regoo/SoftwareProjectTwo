package notify;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class Subject {

	private List<Observer> observers = new ArrayList<Observer>();
	private String state;
	private String email1;
	private String email2;
	private Timestamp date;

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getEmail1() {
		return email1;
	}

	public Subject() 
	{
		new Stud(this);
		new Tech(this);
		
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
		if(state.equals("Tcomment"))
		{
			notifystudent();
		}
		else if(state.equals("Scomment"))
		{
			notifyteacher();
		}
		else
		{
			notifyAllObservers();
		}
	}
	
	 public void attach(Observer observer){
	      observers.add(observer);		
	   }
	
	public void notifyAllObservers() {
		for (Observer observer : observers) {
			observer.updateall();
		}
	}
	
	public void notifystudent() {
		observers.get(0).update();
	}
	
	public void notifyteacher() {
		observers.get(1).update();
	}
}