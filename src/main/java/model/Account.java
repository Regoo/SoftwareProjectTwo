package model;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;

import controller.CommentDBCon;
import controller.CourseDBCon;
import controller.GameDBCon;

public class Account 
{	
	private String Name;
	private String Email;
	private String Password;
	private String Country;
	private String Role;
	private String Phone;
	private int Age;
	private int state;
	private Timestamp date;
	
	public Game PlayGame(String c) throws IOException, ClassNotFoundException
	{
		GameDBCon game=new GameDBCon();
		Game g=game.selectGame(c);
		Game gm=game.selectGame(g);
		return gm;
	}
	
	public ArrayList<Comment>showComments(int id) throws ClassNotFoundException
	{
		CommentDBCon comment =new CommentDBCon();
		ArrayList<Comment> c=new ArrayList<Comment>();
		c=comment.selectComment(id);
		return c;
	}
	
	public ArrayList<Course>showCourses() throws ClassNotFoundException
	{
		CourseDBCon course =new CourseDBCon();
		ArrayList<Course> c=new ArrayList<Course>();
		c=course.selectCourses();
		return c;
	}
	
	public ArrayList<Game>showgame(String c) throws ClassNotFoundException
	{
		CourseDBCon course =new CourseDBCon();
		Course cc = course.selectCourse(c);
		GameDBCon game=new GameDBCon();
		ArrayList<Game> g=game.showGame(cc.getId());
		return g;
	}
	
	public void WriteComment(Comment comment) throws ClassNotFoundException
	{
		CommentDBCon c=new CommentDBCon();
		c.addComment(comment);
	}
	
	public Account(String name, String email, String password, String country,
			String type, String phone, int age) {
		//super();
		Name = name;
		Email = email;
		Password = password;
		Country = country;
		Role = type;
		Phone = phone;
		Age = age;
	}
	public Account() {
		// TODO Auto-generated constructor stub
	}
	
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public String getCountry() {
		return Country;
	}
	public void setCountry(String country) {
		Country = country;
	}
	public String getRole() {
		return Role;
	}
	public void setRole(String type) {
		Role = type;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	public int getAge() {
		return Age;
	}
	public void setAge(int age) {
		Age = age;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

}

