package com.soft.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;

import model.CollaboratorDB;
import model.Comment;
import model.Course;
import model.Game;
import model.MCQ;
import model.Notify;
import model.NotifyDB;
import model.TrueFalse;
import model.User;
import notify.Subject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controller.AccountCon;
import controller.AccountDBCon;
import controller.CommentDBCon;
import controller.CourseDBCon;
import controller.GameDBCon;

@Controller
public class ProjectController {
	User user;

	@RequestMapping("/")
	public String index() {
		user = new User();
		return "webpage";
	}

	@RequestMapping("/regisiter")
	public ModelAndView viewResult1(@RequestParam("name1") String name,
			@RequestParam("email") String email,
			@RequestParam("password") String pass,
			@RequestParam("age") int age, @RequestParam("phone") String phone,
			@RequestParam("country") String country,
			@RequestParam("role") String role, Model model)
			throws ClassNotFoundException {
		email = email.toLowerCase();
		ModelAndView mv = new ModelAndView();
		AccountCon acc = new AccountCon(name, email, pass, country, role,
				phone, age);
		if (user.registeration(acc)) {
			ArrayList<Course> c = new ArrayList<Course>();
			c = user.getAccount().showCourses();
			mv.setViewName("selectCourse");
			mv.addObject("courses", c);
			if (role.equals("student")) {
				mv.addObject("student", user);
			} else {
				mv.addObject("teacher", user);
			}
		} else {
			mv.setViewName("webpage");
			mv.addObject("error", "this email not");
		}
		return mv;

	}

	@RequestMapping("/login")
	public ModelAndView viewResult(@RequestParam("name12") String name,
			@RequestParam("pass") String pass, Model model)
			throws ClassNotFoundException {
		name = name.toLowerCase();
		ModelAndView mv = new ModelAndView();
		if (user.login(name, pass)) {
			ArrayList<Course> c = new ArrayList<Course>();
			if(user.getAccount().getState()==1)
			{
				NotifyDB n=new NotifyDB();
				ArrayList<String> nn=n.selectComment(user.getAccount().getDate(), user.getAccount().getEmail());
				mv.addObject("notif",nn);
			}
			c = user.getAccount().showCourses();
			mv.setViewName("selectCourse");
			mv.addObject("courses", c);
			if (user.getAccount().getRole().equals("student")) {
				mv.addObject("student", user);
			} else {
				mv.addObject("teacher", user);
			}
		} else {
			mv.setViewName("webpage");
			mv.addObject("error1", "Email or password is wrong");
		}

		return mv;
	}

	@RequestMapping("/addcourse")
	public ModelAndView viewResult3(@RequestParam("namee") String name,
			@RequestParam("descr") String disc, Model model)
			throws ClassNotFoundException {
		Course course = new Course();
		course.setName(name);
		course.setDiscribtion(disc);
		course.setEmail(user.getAccount().getEmail());
		user.getAccount1().addCourse(course);
		ModelAndView mv = new ModelAndView();
		mv.setViewName("selectCourse");
		mv.addObject("teacher", user);
		ArrayList<Course> cc = new ArrayList<Course>();
		cc = user.getAccount().showCourses();
		mv.addObject("courses", cc);

		return mv;
	}

	@RequestMapping("/selectgame")
	public ModelAndView viewResult2(@RequestParam("course1") String c,
			Model model) throws ClassNotFoundException {
		user.namec = c;
		ArrayList<Game> g = user.getAccount().showgame(c);
		ModelAndView mv = new ModelAndView();
		if (user.getAccount().getRole().equals("student")) {
			mv.addObject("student", user);
		} else {
			mv.addObject("teacher", user);
		}
		for (int i = 0; i < g.size(); i++) {
			if (g.get(i).getState() == 0) {
				if (!(g.get(i).getEmailOfPublicher().equals(user.getAccount()
						.getEmail()))) {
					g.remove(i);
					i--;
				}
			}
		}
		CourseDBCon course = new CourseDBCon();
		Course cc = course.selectCourse(c);
		mv.addObject("course", cc);
		mv.addObject("games", g);
		mv.setViewName("selectgame");
		return mv;
	}

	@RequestMapping("/game")
	public ModelAndView viewResult5(@RequestParam("game1") String c,Model model)
			throws ClassNotFoundException, IOException {
		
		Game gm = user.getAccount().PlayGame(c);
		user.nameg = gm.getName();
		ModelAndView mv = new ModelAndView();
		if (user.getAccount().getRole().equals("student")) {
			mv.addObject("student", user);
		} else {
			mv.addObject("teacher", user);
			CollaboratorDB x = new CollaboratorDB();
			ArrayList<String> xx = x.cobemail(gm.getID());
			boolean flag = false;
			for (int i = 0; i < xx.size(); i++) {
				if (xx.get(i).equals(user.getAccount().getEmail())) {
					flag = true;
					break;
				}
			}
			if (flag) {
				mv.addObject("edit", "f");
			}
			if (user.getAccount().getEmail().equals(gm.getEmailOfPublicher())) {
				if (gm.getState() == 0) {
					mv.addObject("undo", "f");
				} else {
					mv.addObject("do", "g");
				}
				mv.addObject("edit", "g");
				mv.addObject("self", "d");
				AccountDBCon h = new AccountDBCon();
				ArrayList<String> j = h.selectteacher(user.getAccount()
						.getEmail());
				mv.addObject("account", j);
			}
			else
			{
				mv.addObject("copy","");
			}
		}
		if (gm.getType().equals("TrueFalse")) {
			mv.setViewName("tf");
			int size = gm.getTf().size();
			TrueFalse[] t = new TrueFalse[size];
			for (int i = 0; i < size; i++) {
				t[i] = gm.getTf().get(i);
			}
			mv.addObject("truefalse", t);
		} else {
			mv.setViewName("mcq");
			int size = gm.getMcq().size();
			MCQ[] m = new MCQ[size];
			for (int i = 0; i < size; i++) {
				m[i] = gm.getMcq().get(i);
			}
			mv.addObject("mcq", m);
		}
		mv.addObject("game", gm);
		mv.addObject("play", 5);
		return mv;
	}

	@RequestMapping("/cob")
	public ModelAndView viewResult13(@RequestParam("temail") String e,
			@RequestParam("Id") int id, Model model)
			throws ClassNotFoundException, IOException {
		Game gm = user.getAccount().PlayGame(user.nameg);
		ModelAndView mv = new ModelAndView();
		CollaboratorDB d = new CollaboratorDB();
		d.addcollaborator(e, id);
		mv.addObject("teacher", user);
		mv.addObject("edit", "g");
		mv.addObject("self", "d");
		if (gm.getState() == 0) {
			mv.addObject("undo", "f");
		} else {
			mv.addObject("do", "g");
		}
		AccountDBCon h = new AccountDBCon();
		ArrayList<String> j = h.selectteacher(user.getAccount().getEmail());
		mv.addObject("account", j);
		if (gm.getType().equals("TrueFalse")) {
			mv.setViewName("tf");
			int size = gm.getTf().size();
			TrueFalse[] t = new TrueFalse[size];
			for (int i = 0; i < size; i++) {
				t[i] = gm.getTf().get(i);
			}
			mv.addObject("truefalse", t);
		} else {
			mv.setViewName("mcq");
			int size = gm.getMcq().size();
			MCQ[] m = new MCQ[size];
			for (int i = 0; i < size; i++) {
				m[i] = gm.getMcq().get(i);
			}
			mv.addObject("mcq", m);
		}
		mv.addObject("game", gm);
		mv.addObject("play", 5);
		return mv;
	}
	
	@RequestMapping("/copy")
	public ModelAndView viewResult16(Model model)
			throws ClassNotFoundException, IOException {
		Game gm = user.getAccount().PlayGame(user.nameg);
		ModelAndView mv = new ModelAndView();
		CollaboratorDB x = new CollaboratorDB();
		ArrayList<String> xx = x.cobemail(gm.getID());
		boolean flag = false;
		for (int i = 0; i < xx.size(); i++) {
			if (xx.get(i).equals(user.getAccount().getEmail())) {
				flag = true;
				break;
			}
		}
		if (flag) {
			mv.addObject("edit", "f");
		}
		if (user.getAccount().getEmail().equals(gm.getEmailOfPublicher())) {
			//hhh
		}
		else
		{
			mv.addObject("copy","");
		}
		mv.addObject("teacher", user);
		user.getAccount1().copygame(gm.getID(), user.getAccount().getEmail());
		if (gm.getType().equals("TrueFalse")) {
			mv.setViewName("tf");
			int size = gm.getTf().size();
			TrueFalse[] t = new TrueFalse[size];
			for (int i = 0; i < size; i++) {
				t[i] = gm.getTf().get(i);
			}
			mv.addObject("truefalse", t);
		} else {
			mv.setViewName("mcq");
			int size = gm.getMcq().size();
			MCQ[] m = new MCQ[size];
			for (int i = 0; i < size; i++) {
				m[i] = gm.getMcq().get(i);
			}
			mv.addObject("mcq", m);
		}
		mv.addObject("game", gm);
		mv.addObject("play", 5);
		return mv;
	}

	@RequestMapping("/cancel")
	public ModelAndView viewResult14(Model model)
			throws ClassNotFoundException, IOException {
		Game gm = user.getAccount().PlayGame(user.nameg);
		ModelAndView mv = new ModelAndView();
		GameDBCon d = new GameDBCon();
		d.updatestate(0, gm.getID());
		gm.setState(0);
		mv.addObject("teacher", user);
		mv.addObject("edit", "g");
		mv.addObject("self", "d");
		if (gm.getState() == 0) {
			mv.addObject("undo", "f");
		} else {
			mv.addObject("do", "g");
		}
		AccountDBCon h = new AccountDBCon();
		ArrayList<String> j = h.selectteacher(user.getAccount().getEmail());
		mv.addObject("account", j);
		if (gm.getType().equals("TrueFalse")) {
			mv.setViewName("tf");
			int size = gm.getTf().size();
			TrueFalse[] t = new TrueFalse[size];
			for (int i = 0; i < size; i++) {
				t[i] = gm.getTf().get(i);
			}
			mv.addObject("truefalse", t);
		} else {
			mv.setViewName("mcq");
			int size = gm.getMcq().size();
			MCQ[] m = new MCQ[size];
			for (int i = 0; i < size; i++) {
				m[i] = gm.getMcq().get(i);
			}
			mv.addObject("mcq", m);
		}
		mv.addObject("game", gm);
		mv.addObject("play", 5);
		return mv;
	}

	@RequestMapping("/uncancel")
	public ModelAndView viewResult15(Model model)
			throws ClassNotFoundException, IOException {
		Game gm = user.getAccount().PlayGame(user.nameg);
		ModelAndView mv = new ModelAndView();
		GameDBCon d = new GameDBCon();
		d.updatestate(1, gm.getID());
		gm.setState(1);
		mv.addObject("teacher", user);
		mv.addObject("edit", "g");
		mv.addObject("self", "d");
		if (gm.getState() == 0) {
			mv.addObject("undo", "f");
		} else {
			mv.addObject("do", "g");
		}
		AccountDBCon h = new AccountDBCon();
		ArrayList<String> j = h.selectteacher(user.getAccount().getEmail());
		mv.addObject("account", j);
		if (gm.getType().equals("TrueFalse")) {
			mv.setViewName("tf");
			int size = gm.getTf().size();
			TrueFalse[] t = new TrueFalse[size];
			for (int i = 0; i < size; i++) {
				t[i] = gm.getTf().get(i);
			}
			mv.addObject("truefalse", t);
		} else {
			mv.setViewName("mcq");
			int size = gm.getMcq().size();
			MCQ[] m = new MCQ[size];
			for (int i = 0; i < size; i++) {
				m[i] = gm.getMcq().get(i);
			}
			mv.addObject("mcq", m);
		}
		mv.addObject("game", gm);
		mv.addObject("play", 5);
		return mv;
	}

	@RequestMapping("/tfresult")
	public ModelAndView viewResult6(@RequestParam("q1") String c1,
			@RequestParam("q2") String c2, @RequestParam("q3") String c3,
			@RequestParam("q4") String c4, @RequestParam("q5") String c5,
			Model model) throws ClassNotFoundException, IOException {
		Game gm = user.getAccount().PlayGame(user.nameg);
		ModelAndView mv = new ModelAndView();
		if (user.getAccount().getRole().equals("student")) {
			mv.addObject("student", user);
		} else {
			mv.addObject("teacher", user);
		}
		mv.setViewName("tf");
		int size = gm.getTf().size();
		TrueFalse[] t = new TrueFalse[size];
		for (int i = 0; i < size; i++) {
			t[i] = gm.getTf().get(i);
		}
		int mark = 0;
		if (c1.equals(t[0].getAnswer())) {
			mark += 10;
		}
		if (c2.equals(t[1].getAnswer())) {
			mark += 10;
		}
		if (c3.equals(t[2].getAnswer())) {
			mark += 10;
		}
		if (c4.equals(t[3].getAnswer())) {
			mark += 10;
		}
		if (c5.equals(t[4].getAnswer())) {
			mark += 10;
		}
		if (mark >= gm.getHighScore()) {
			gm.setHighScore(mark);
			gm.setNameHighScore(user.getAccount().getName());
			GameDBCon x = new GameDBCon();
			x.updatehighscore(mark, user.getAccount().getName(), gm.getID());
		}
		mv.addObject("game", gm);
		mv.addObject("degree", mark);
		ArrayList<Comment> comment = new ArrayList<Comment>();
		CommentDBCon com = new CommentDBCon();
		comment = com.selectComment(gm.getID());
		mv.addObject("comments", comment);
		mv.addObject("scoree", 5);
		return mv;
	}

	@RequestMapping("/addcomment")
	public ModelAndView viewResult7(@RequestParam("comment1") String c,
			@RequestParam("to") String e, Model model)
			throws ClassNotFoundException, IOException {
		Comment comment = new Comment();
		comment.setEmail(user.getAccount().getEmail());
		comment.setText(c);
		Game gm = user.getAccount().PlayGame(user.nameg);
		comment.setId(gm.getID());
		CommentDBCon cc = new CommentDBCon();
		cc.addComment(comment);
		ModelAndView mv = new ModelAndView();
		if (user.getAccount().getRole().equals("student")) {
			mv.addObject("student", user);
		} else {
			mv.addObject("teacher", user);
		}
		if (gm.getType().equals("TrueFalse")) {
			mv.setViewName("tf");
		} else {
			mv.setViewName("mcq");
		}
		mv.addObject("scoree", 5);
		mv.addObject("game", gm);
		ArrayList<Comment> comments = new ArrayList<Comment>();
		comments = cc.selectComment(gm.getID());
		mv.addObject("comments", comments);
		// notify
		Notify n = new Notify();
		Subject s = new Subject();
		Timestamp date = new Timestamp(new java.util.Date().getTime());
		n.setDate(date);
		s.setDate(date);
		if (user.getAccount().getRole().equals("teacher")) {
			n.setDiscription(user.getAccount().getName()
					+ " replay acomment on  game " + user.nameg + " on course "
					+ user.namec);
			n.setId(e);
			s.setEmail1(user.getAccount().getEmail());
			s.setEmail2(e);
			s.setState("Tcomment");
		} else {
			n.setDiscription(user.getAccount().getName()
					+ " write acomment on your game " + user.nameg
					+ " on course " + user.namec);
			n.setId(gm.getEmailOfPublicher());
			s.setEmail1(user.getAccount().getEmail());
			s.setEmail2(gm.getEmailOfPublicher());
			s.setState("Scomment");
		}
		NotifyDB nn = new NotifyDB();
		nn.addNotify(n);

		return mv;
	}

	@RequestMapping("/mcqresult")
	public ModelAndView viewResult8(@RequestParam("q1") int c1,
			@RequestParam("q2") int c2, @RequestParam("q3") int c3,
			@RequestParam("q4") int c4, @RequestParam("q5") int c5, Model model)
			throws ClassNotFoundException, IOException {
		Game gm = user.getAccount().PlayGame(user.nameg);
		ModelAndView mv = new ModelAndView();
		if (user.getAccount().getRole().equals("student")) {
			mv.addObject("student", user);
		} else {
			mv.addObject("teacher", user);
		}
		mv.setViewName("mcq");
		int size = gm.getMcq().size();
		MCQ[] t = new MCQ[size];
		for (int i = 0; i < size; i++) {
			t[i] = gm.getMcq().get(i);
		}
		int mark = 0;
		if (c1 == (t[0].getAnswer())) {
			mark += 10;
		}
		if (c2 == (t[1].getAnswer())) {
			mark += 10;
		}
		if (c3 == (t[2].getAnswer())) {
			mark += 10;
		}
		if (c4 == (t[3].getAnswer())) {
			mark += 10;
		}
		if (c5 == (t[4].getAnswer())) {
			mark += 10;
		}
		if (mark >= gm.getHighScore()) {
			gm.setHighScore(mark);
			gm.setNameHighScore(user.getAccount().getName());
			GameDBCon x = new GameDBCon();
			x.updatehighscore(mark, user.getAccount().getName(), gm.getID());
		}
		mv.addObject("game", gm);
		mv.addObject("degree", mark);
		ArrayList<Comment> comment = new ArrayList<Comment>();
		CommentDBCon com = new CommentDBCon();
		comment = com.selectComment(gm.getID());
		mv.addObject("comments", comment);
		mv.addObject("scoree", 5);
		return mv;
	}

	@RequestMapping("/gametf")
	public ModelAndView viewResult9(@RequestParam("name1") String name,
			@RequestParam("form_select") String type,
			@RequestParam("q1") String c1, @RequestParam("q11") String c11,
			@RequestParam("q2") String c2, @RequestParam("q21") String c21,
			@RequestParam("q3") String c3, @RequestParam("q31") String c31,
			@RequestParam("q4") String c4, @RequestParam("q41") String c41,
			@RequestParam("q5") String c5, @RequestParam("q51") String c51,
			Model model) throws ClassNotFoundException, IOException {
		ArrayList<TrueFalse> t = new ArrayList<TrueFalse>();
		TrueFalse tt = new TrueFalse();
		tt.setQuestion(c1);
		tt.setAnswer(c11);
		t.add(tt);
		tt = new TrueFalse();
		tt.setQuestion(c2);
		tt.setAnswer(c21);
		t.add(tt);
		tt = new TrueFalse();
		tt.setQuestion(c3);
		tt.setAnswer(c31);
		t.add(tt);
		tt = new TrueFalse();
		tt.setQuestion(c4);
		tt.setAnswer(c41);
		t.add(tt);
		tt = new TrueFalse();
		tt.setQuestion(c5);
		tt.setAnswer(c51);
		t.add(tt);
		CourseDBCon course = new CourseDBCon();
		Course s = new Course();
		s = course.selectCourse(user.namec);
		Game gm = new Game();
		gm.setEmailOfPublicher(user.getAccount().getEmail());
		gm.setName(name);
		gm.setType(type);
		gm.setTf(t);
		gm.setIDCourse(s.getId());
		user.getAccount1().AddGame(gm);

		ModelAndView mv = new ModelAndView();
		if (user.getAccount().getRole().equals("student")) {
			mv.addObject("student", user);
		} else {
			mv.addObject("teacher", user);
		}
		mv.addObject("course", s);
		ArrayList<Game> g = user.getAccount().showgame(user.namec);
		mv.addObject("games", g);
		mv.setViewName("selectgame");

		// notify
		Notify n = new Notify();
		n.setDiscription(user.getAccount().getName() + " add game " + name
				+ " on course " + user.namec);
		n.setId("nnn");
		Timestamp date = new Timestamp(new java.util.Date().getTime());
		n.setDate(date);
		NotifyDB nn = new NotifyDB();
		nn.addNotify(n);
		Subject ss = new Subject();
		ss.setDate(date);
		ss.setEmail1(user.getAccount().getEmail());
		ss.setState("add");
		return mv;
	}

	@RequestMapping("/gamemcq")
	public ModelAndView viewResult10(@RequestParam("name1") String name,
			@RequestParam("form_select") String type,
			@RequestParam("q1") String c1, @RequestParam("q11") String c11,
			@RequestParam("q12") String c12, @RequestParam("q13") String c13,
			@RequestParam("q14") String c14, @RequestParam("ans1") int ans1,
			@RequestParam("q2") String c2, @RequestParam("q21") String c21,
			@RequestParam("q22") String c22, @RequestParam("q23") String c23,
			@RequestParam("q24") String c24, @RequestParam("ans2") int ans2,
			@RequestParam("q3") String c3, @RequestParam("q31") String c31,
			@RequestParam("q32") String c32, @RequestParam("q33") String c33,
			@RequestParam("q34") String c34, @RequestParam("ans3") int ans3,
			@RequestParam("q41") String c4, @RequestParam("q41") String c41,
			@RequestParam("q42") String c42, @RequestParam("q43") String c43,
			@RequestParam("q44") String c44, @RequestParam("ans4") int ans4,
			@RequestParam("q5") String c5, @RequestParam("q51") String c51,
			@RequestParam("q52") String c52, @RequestParam("q53") String c53,
			@RequestParam("q54") String c54, @RequestParam("ans5") int ans5

			, Model model) throws ClassNotFoundException, IOException {
		ArrayList<MCQ> t = new ArrayList<MCQ>();
		MCQ tt = new MCQ();
		ArrayList<String> a = new ArrayList<String>();
		tt.setQuestion(c1);
		tt.setAnswer(ans1);
		a.add(c11);
		a.add(c12);
		a.add(c13);
		a.add(c14);
		tt.setChoice(a);
		t.add(tt);
		tt = new MCQ();
		a = new ArrayList<String>();

		tt.setQuestion(c2);
		tt.setAnswer(ans2);
		a.add(c21);
		a.add(c22);
		a.add(c23);
		a.add(c24);
		tt.setChoice(a);
		t.add(tt);
		tt = new MCQ();
		a = new ArrayList<String>();

		tt.setQuestion(c3);
		tt.setAnswer(ans3);
		a.add(c31);
		a.add(c32);
		a.add(c33);
		a.add(c34);
		tt.setChoice(a);
		t.add(tt);
		tt = new MCQ();
		a = new ArrayList<String>();

		tt.setQuestion(c4);
		tt.setAnswer(ans4);
		a.add(c41);
		a.add(c42);
		a.add(c43);
		a.add(c44);
		tt.setChoice(a);
		t.add(tt);
		tt = new MCQ();
		a = new ArrayList<String>();

		tt.setQuestion(c5);
		tt.setAnswer(ans5);
		a.add(c51);
		a.add(c52);
		a.add(c53);
		a.add(c54);
		tt.setChoice(a);
		t.add(tt);
		tt = new MCQ();
		a = new ArrayList<String>();

		CourseDBCon course = new CourseDBCon();
		Course s = new Course();
		s = course.selectCourse(user.namec);
		Game gm = new Game();
		gm.setEmailOfPublicher(user.getAccount().getEmail());
		gm.setName(name);
		gm.setType(type);
		gm.setMcq(t);
		gm.setIDCourse(s.getId());
		user.getAccount1().AddGame(gm);

		ModelAndView mv = new ModelAndView();
		if (user.getAccount().getRole().equals("student")) {
			mv.addObject("student", user);
		} else {
			mv.addObject("teacher", user);
		}
		mv.addObject("course", s);
		ArrayList<Game> g = user.getAccount().showgame(user.namec);
		mv.addObject("games", g);
		mv.setViewName("selectgame");

		// notify
		Notify n = new Notify();
		n.setDiscription(user.getAccount().getName() + " add game " + name
				+ " on course " + user.namec);
		n.setId("nnn");
		Timestamp date = new Timestamp(new java.util.Date().getTime());
		n.setDate(date);
		NotifyDB nn = new NotifyDB();
		nn.addNotify(n);
		Subject ss = new Subject();
		ss.setDate(date);
		ss.setEmail1(user.getAccount().getEmail());
		ss.setState("add");

		return mv;
	}

	@RequestMapping("/home")
	public ModelAndView viewResult11(Model model) throws ClassNotFoundException {
		ModelAndView mv = new ModelAndView();
		ArrayList<Course> c = new ArrayList<Course>();
		c = user.getAccount().showCourses();
		mv.setViewName("selectCourse");
		mv.addObject("courses", c);
		if (user.getAccount().getRole().equals("student")) {
			mv.addObject("student", user);
		} else {
			mv.addObject("teacher", user);
		}

		return mv;
	}

	@RequestMapping("/log")
	public ModelAndView viewResult12(Model model) throws ClassNotFoundException {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("webpage");
		user = new User();
		return mv;
	}
}
